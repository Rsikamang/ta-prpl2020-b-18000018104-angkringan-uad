<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Tugas Proyek</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Fotog Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=EB+Garamond:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
    <!-- //online-fonts -->
</head>
<body>
    <!-- banner -->
    <div class="inner-banner">
        <!-- header -->
			  <div class="top-head py-3">
                    
               </div>
            <header>	
            <nav class="mnu navbar-light">
            <div class="logo" id="logo">
                <h1><a href="index.php">Angkringan UAD</a></h1>
            </div>
				<label for="drop" class="toggle"><span class="fa fa-bars"></span></label>
                <input type="checkbox" id="drop">
                <ul class="menu">
                        <li class="mr-lg-4 mr-3"><a href="index.php">Home</a></li>
                        <li class="mr-lg-4 mr-3"><a href="pelanggan.php">Pelanggan</a></li>
                        <li class="mr-lg-4 mr-3"><a href="menu.php">Menu</a></li>
                        <li class="mr-lg-4 mr-3"><a href="Pembayaran.php">Pembayaran</a></li>
                        <li class="mr-lg-4 mr-3"><a href="penjual.php">Penjualan</a></li>
                </ul>
    </nav>
</header>
        <!-- //header -->
    </div>
    <!-- //banner -->
     <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Pelanggan</li>
        </ol>
    </nav>
	<!-- gallery -->
    <section class="advantages pt-4">
	<div class="container pb-md-4">
	<table class="table table-bordered">
		<tr>
            <th>NO</th>
            <th>Id Pelanggan</th>
			<th>Nama</th>
			<th>No Hp</th>
            <th>Alamat</th>
			<th>OPSI</th>

		</tr>
		<?php 
		include 'koneksi.php';
		$no = 1;
		$data = mysqli_query($koneksi,"select * from pelanggan");
		while($d = mysqli_fetch_array($data)){
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $d['id_pelanggan']; ?></td>
				<td><?php echo $d['nama']; ?></td>
				<td><?php echo $d['no_hp']; ?></td>
                <td><?php echo $d['alamat']; ?></td>
				<td>
					<a href="editpelanggan.php?id_pelanggan=<?php echo $d['id_pelanggan']; ?>">EDIT</a> || 
					<a href="hapuspelanggan.php?id_pelanggan=<?php echo $d['id_pelanggan']; ?>">HAPUS</a>
				</td>
			</tr>
			<?php 
		}
		?>
		  </table>
		  <a href="tambahpelanggan.php"> <button class="add" class="btn btn-primary">Tambah Data</button></a>
	</div>
</section>
   
<div class="cpy-right text-center  py-3">
        <p class="text-white">Rahmayani Sikamang - 1800018104</a>
        </p>
    </div>
   
</body>
</html>