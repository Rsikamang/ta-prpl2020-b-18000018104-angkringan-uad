<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ANGKRINGAN UAD</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/business-casual.min.css" rel="stylesheet">

</head>

<body>

  <h1 class="site-heading text-center text-white d-none d-lg-block">
    <span class="site-heading-upper text-primary mb-3">ANGKRINGAN</span>
    <span class="site-heading-lower">UNIVERSITAS AHMAD DAHLAN</span>
  </h1>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
      <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="index.html">Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
           <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="http://localhost/angkringanuad/login/index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="http://localhost/angkringanuad/menu/index.php">Menu</a>
            </li>
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="http://localhost/angkringanuad/pelanggan/index.php">Pelanggan</a>
            </li>
            <li class="nav-item active px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="http://localhost/angkringanuad/penjualan/index.php">Penjualan</a>
            </li>
             <li class="nav-item active px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="http://localhost/angkringanuad/pembayaran/index.php">Pembayaran</a>
            </li>
            <li class="nav-item active px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="http://localhost/angkringanuad/login/logout.php">Keluar</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

  <section class="page-section about-heading">
    <div class="container">
      <div class="about-heading-content">
        <div class="row">
          <div class="col-xl-9 col-lg-10 mx-auto">
            <div class="bg-faded rounded p-5">
              <h2 class="section-heading mb-4">
                <span class="section-heading-lower">EDIT DATA MENU</span>
              </h2>
              <p>

    <?php
    include 'koneksi.php';
    $kode_produk = $_GET['kode_produk'];
    $data = mysqli_query($koneksi,"select * from menu where kode_produk='$kode_produk'");
    while($d = mysqli_fetch_array($data)){
        ?>
        <form method="post" action="updatemenu.php">
            <table>
                <tr>
                    <td>Kode Produk</td>
                    <td><input type="text" name="kode_produk" value="<?php echo $d['kode_produk']; ?>"></td>
                </tr>
                <tr>
                    <td>Nama Makanan</td>
                    <td><input type="text" name="nama" value="<?php echo $d['nama_makanan']; ?>"></td>
                </tr>
                <tr>
                    <td>Jenis</td>
                    <td><input type="text" name="jenis" value="<?php echo $d['jenis']; ?>"></td>
                </tr>
                <tr>
                    <td>Harga</td>
                    <td><input type="text" name="harga" value="<?php echo $d['harga']; ?>"></td>
                </tr>
                <div class="form-row align-items-center">
    <div class="col-auto my-3">
  
        <label for="validationTooltip01">Id Pelanggan </label>
        <select class="form-control" id="validationTooltip01" name="id_pelanggan">
        <?php 
            include 'koneksi.php';
            $data=mysqli_query($koneksi,"SELECT id_pelanggan from pelanggan");
            while ($d=mysqli_fetch_array($data)) {
        ?>
            <option value="<?=$d['id_pelanggan']?>"><?=$d['id_pelanggan']?></option> 
        <?php
            }
        ?>
        </select>
        </div>
        </div>
                <tr>
                    <td></td>
                    <td><input type="submit" value="SIMPAN"></td>
                </tr>       
            </table>
        </form>
        <?php 
    }
    ?>
          </table>

              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <footer class="footer text-faded text-center py-5">
    <div class="container">
      <p class="m-0 small">Copyright &copy; Your Website 2019</p>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
